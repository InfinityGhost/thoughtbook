var title = document.getElementById("title")
var name = document.getElementById("name")

var quill = new Quill("#editor", { // eslint-disable-line no-undef
    theme: "bubble"
})

function publish() {
    var story = document.querySelector(".ql-editor").innerHTML 
    if (!title.value) return document.getElementById("err").style.display = "inherit"
    fetch("/publish", {
        method: "POST",
        headers: {
            "User-Agent": "Thoughtbook Web GUI/latest",
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            title: title.value,
            name: name.value,
            story: story
        })
    }).then(r=>r.text()).then(r_=> {
        document.location.href = r_
    })
}