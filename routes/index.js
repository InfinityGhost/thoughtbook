var express = require('express');
var router = express.Router();

var Datastore = require("nedb")
var pub = new Datastore({
  filename: "publishings.db"
})

var flakes = (require("discord.js")).SnowflakeUtil

pub.loadDatabase()

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', { title: 'Thoughtbook', post: true });
});

router.post("/publish", (req, res, next) => {
  if (!req.body.title || !req.body.story) return res.status(400).send("I need a title and any content!")
  let name = req.body.name
  let th = { ...req.body }
  th._id = flakes.generate(Date.now())
  th.name = name
  pub.insert(th)
  res.send(req.protocol + "://" + req.get("host") + "/view/" + th._id)
})

router.get("/view", (req,res,next) => {
  res.render("error", {
    message: "Not sure what to view",
    error: {
      "status": "No ID specified."
    }
  })
})

router.get("/view/:snowflake", (req, res, next) => {
  if (!req.params.snowflake) return res.render("error", {
    message: "Not sure what to view",
    error: {
      "status": "No ID specified."
    }
  })
  pub.find({_id:req.params.snowflake}, (err, resp) => {
    var doc = resp[0]
    if (!doc) return res.render("error", {
      message: "Nothing found.",
      error: {
        "status": "Check the ID."
      }
    })
    res.render("post", {
      title: "Thoughtbook",
      post: doc,
      date: flakes.deconstruct(doc._id).date.toLocaleDateString("en-US", {
        weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'
      })
    })
  })
})

module.exports = router;
